<?php
namespace App\Model;

use App\VO\Money;

class DaemonAd extends BaseAd
{
    /** @var int */
    private $campaignId;

    /** @var int */
    private $userId;

    public function __construct(
        int $id,
        string $name,
        string $description,
        Money $price,
        int $userId,
        int $campaignId
    ) {
        parent::__construct($id, $name, $description, $price);

        $this->campaignId = $campaignId;
        $this->userId = $userId;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }

    public function getCampaignId(): int
    {
        return $this->campaignId;
    }
}
