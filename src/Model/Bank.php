<?php
namespace App\Model;

use App\VO\Currency;
use App\VO\Money;

class Bank implements BankInterface, CurrencyValidatorInterface
{
    /** @var array<string, array> */
    private $currencies;

    /**
     * @param array<string, array> $rates
     */
    public function __construct(array $currenciesConfig)
    {
        $this->currencies = array_change_key_case($currenciesConfig, CASE_UPPER);
    }

    public function convertMoney(Money $money, Currency $to): Money
    {
        $newAmount = $money->getAmount() * $this->getRate($money->getCurrency(), $to);

        return new Money($newAmount, $to);
    }

    public function formatMoney(Money $money): string
    {
        return sprintf(
            $this->currencies[$money->getCurrency()->getCode()]['format'],
            $money->getAmount()
        );
    }

    public function validateCurrency(Currency $currency): bool
    {
        return array_key_exists($currency->getCode(), $this->currencies);
    }

    private function getRate(Currency $from, Currency $to): float
    {
        return $this->getRateToInternal($from) / $this->getRateToInternal($to);
    }

    private function getRateToInternal(Currency $currency): float
    {
        return $this->currencies[$currency->getCode()]['rate'];
    }
}
