<?php
namespace App\Model;

class HtmlAdViewer implements AdViewer
{
    /**
     * {@inheritdoc}
     */
    public function render(AdInterface $ad, array $params = []): string
    {
        return sprintf(
            "<h1>%s</h1>\n".
            "<p>%s</p>\n".
            "<p>стоимость: %s</p>",
            $ad->getName(),
            $ad->getDescription(),
            $ad->getPrice()->formatAs($params['view_currency'] ?? 'USD')
        );
    }
}
