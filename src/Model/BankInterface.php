<?php
namespace App\Model;

use App\VO\Currency;
use App\VO\Money;

interface BankInterface
{
    public function convertMoney(Money $money, Currency $to): Money;
    public function formatMoney(Money $money): string;
}
