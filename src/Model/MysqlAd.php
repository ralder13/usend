<?php
namespace App\Model;

use App\VO\Money;

class MysqlAd extends BaseAd
{
    /** @var string */
    private $keywords;

    public function __construct(
        int $id,
        string $name,
        string $description,
        Money $price,
        string $keywords
    ) {
        parent::__construct($id, $name, $description, $price);

        $this->keywords = $keywords;
    }

    public function getKeywords(): string
    {
        return $this->keywords;
    }
}
