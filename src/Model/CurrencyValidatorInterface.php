<?php
namespace App\Model;

use App\VO\Currency;

interface CurrencyValidatorInterface
{
    /**
     * @param Currency $currency
     *
     * @return bool
     */
    public function validateCurrency(Currency $currency): bool;
}
