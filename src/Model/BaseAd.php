<?php
namespace App\Model;

use App\VO\Money;

abstract class BaseAd implements AdInterface
{
    /** @var int */
    private $id;

    /** @var string */
    private $name;

    /** @var string */
    private $description;

    /** @var Money */
    private $price;

    public function __construct(
        int $id,
        string $name,
        string $description,
        Money $price
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->price = $price;
    }

    /**
     * {@inheritdoc}
     */
    public function getPrice(): Money
    {
        return $this->price;
    }

    /**
     * {@inheritdoc}
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription(): string
    {
        return $this->description;
    }
}
