<?php
namespace App\Model;

interface AdViewer
{
    public function render(AdInterface $ad, array $params = []): string;
}
