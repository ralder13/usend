<?php
namespace App\Model;

use App\VO\Money;

interface AdInterface
{
    public function getId(): int;
    public function getName(): string;
    public function getDescription(): string;
    public function getPrice(): Money;
}
