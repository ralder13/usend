<?php
namespace App\VO;

use App\Model\BankInterface;
use RuntimeException;

class Money
{
    /** @var float */
    private $amount;

    /** @var Currency */
    private $currency;

    /** @var BankInterface */
    private static $bank;

    public function __construct(float $amount, Currency $currency)
    {
        $this->amount = $amount;
        $this->currency = $currency;
    }

    /**
     * {@inheritdoc}
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * {@inheritdoc}
     */
    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    /**
     * {@inheritdoc}
     */
    public function add(Money $money): Money
    {
        $money = $this->getBank()->convertMoney($money, $this->currency);

        return new Money($this->amount + $money->getAmount(), $this->currency);
    }

    /**
     * {@inheritdoc}
     */
    public function format(): string
    {
        return $this->getBank()->formatMoney($this);
    }

    /**
     * {@inheritdoc}
     */
    public function formatAs(string $currencyCode): string
    {
        $money = $this->getBank()->convertMoney($this, new Currency($currencyCode));

        return $this->getBank()->formatMoney($money);
    }

    /**
     * {@inheritdoc}
     */
    public function multiply(int $multiplier): Money
    {
        return new Money($this->amount*$multiplier, $this->currency);
    }

    /**
     * {@inheritdoc}
     */
    public function sub(Money $money): Money
    {
        $money = $this->getBank()->convertMoney($money, $this->currency);

         return new Money($this->amount - $money->getAmount(), $this->currency);
    }

    public static function registerBank(BankInterface $bank): void
    {
        self::$bank = $bank;
    }

    private function getBank(): BankInterface
    {
        if (null === self::$bank) {
            throw new RuntimeException('Bank not registered');
        }

        return self::$bank;
    }
}
