<?php
namespace App\VO;

use App\Model\CurrencyValidatorInterface;
use RuntimeException;

class Currency
{
    /** @var string */
    private $code;

    /** @var CurrencyValidatorInterface */
    private static $currencyValidator;

    public function __construct(string $code)
    {
        $this->code = strtoupper($code);

        $this->getValidator()->validateCurrency($this);
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public static function registerValidator(CurrencyValidatorInterface $validator): void
    {
        self::$currencyValidator = $validator;
    }

    private function getValidator(): CurrencyValidatorInterface
    {
        if (null === self::$currencyValidator) {
            throw new RuntimeException('Currency validator not registered');
        }

        return self::$currencyValidator;
    }
}
