<?php
namespace App\Service;

use App\Model\AdInterface;
use App\Repository\AdRepositoryFactory;
use RuntimeException;

class AdManager implements AdService
{
    /** @var AdRepositoryFactory */
    private $repositoryFactory;

    public function __construct(AdRepositoryFactory $adRepositoryFactory)
    {
        $this->repositoryFactory = $adRepositoryFactory;
    }

    public function get(int $id, string $from): AdInterface
    {
        $repository = $this->repositoryFactory->getRepository($from);
        $ad = $repository->get($id);
        if (null === $ad) {
            throw new RuntimeException('Ad not found');
        }

        return $ad;
    }
}
