<?php
namespace App\Service;

use App\Model\AdInterface;

interface AdService
{
    public function get(int $id, string $from): AdInterface;
}
