<?php

class FileLogger
{
    private $file;

    public function __construct($path)
    {
        $this->file = fopen($path, 'a');
    }

    public function log($message)
    {
        fwrite($this->file, $message."\n");
    }

    public function __destruct()
    {
        fclose($this->file);
    }
}
