<?php
namespace App\Repository;

use App\Infra\LoggerInterface;
use InvalidArgumentException;

class AdRepositoryFactory
{
    public const FROM_DAEMON = 'daemon';
    public const FROM_MYSQL = 'mysql';

    /** @var LoggerInterface */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function getRepository(string $from): AdRepository
    {
        switch (strtolower($from)) {
            case self::FROM_MYSQL:
                return new MysqlAdRepository($this->logger);
            case self::FROM_DAEMON:
                return new DaemonAdRepository($this->logger);
            default:
                throw new InvalidArgumentException('Invalid from');
        }
    }
}
