<?php
namespace App\Repository;

use App\Infra\LoggerInterface;

abstract class BaseAdRepository
{
    /** @var LoggerInterface */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function getLogger(): LoggerInterface
    {
        return $this->logger;
    }
}
