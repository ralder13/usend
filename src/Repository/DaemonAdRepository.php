<?php
namespace App\Repository;

use App\Model\AdInterface;
use App\VO\Currency;
use App\Model\DaemonAd;
use App\VO\Money;

class DaemonAdRepository extends BaseAdRepository implements AdRepository
{
    public function get(int $id): ?AdInterface
    {
        $this->getLogger()->log(sprintf('get_daemon_ad_info(ID=%d)', $id));

        //TODO: handle errors
        //TODO: handle empty response

        return $this->buildModel(get_deamon_ad_info($id));
    }

    private function buildModel(string $data): AdInterface
    {
        //TODO: validate data
        $parsedData = explode("\t", $data);
        $id = (int) $parsedData[0];
        $campaignId = (int) $parsedData[1];
        $userId = (int) $parsedData[2];
        $name = $parsedData[3];
        $description = $parsedData[4];
        $price = (int) $parsedData[5];
        $price = new Money($price, new Currency('USD'));

        return new DaemonAd($id, $name, $description, $price, $userId, $campaignId);
    }
}
