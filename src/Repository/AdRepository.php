<?php
namespace App\Repository;

use App\Model\AdInterface;

interface AdRepository
{
    public function get(int $id): ?AdInterface;
}
