<?php
namespace App\Repository;

use App\Model\AdInterface;
use App\VO\Currency;
use App\VO\Money;
use App\Model\MysqlAd;

class MysqlAdRepository extends BaseAdRepository implements AdRepository
{
    public function get(int $id): ?AdInterface
    {
        $this->getLogger()->log(sprintf('getAdRecord(ID=%d)', $id));

        //TODO: handle errors
        //TODO: handle empty response

        return $this->buildModel(getAdRecord($id));
    }

    private function buildModel(array $data): AdInterface
    {
        //TODO: validate data
        return new MysqlAd(
            $data['id'],
            $data['name'],
            $data['text'],
            new Money($data['price'], new Currency('USD')),
            $data['keywords']
        );
    }
}
