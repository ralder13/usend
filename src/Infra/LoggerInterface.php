<?php
namespace App\Infra;

interface LoggerInterface
{
    public function log(string $message): void;
}
