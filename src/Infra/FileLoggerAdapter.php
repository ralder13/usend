<?php
namespace App\Infra;

use FileLogger;

class FileLoggerAdapter implements LoggerInterface
{
    /** @var FileLogger */
    private $fileLogger;

    public function __construct(FileLogger $fileLogger)
    {
        $this->fileLogger = $fileLogger;
    }

    public function log(string $message): void
    {
        $time = date('Y-m-d H:i:s');
        $this->fileLogger->log(sprintf('[%s] %s', $time, $message));
    }
}
