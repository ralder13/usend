<?php
namespace App\Infra;

class NullLogger implements LoggerInterface
{
    public function log(string $message): void
    {
    }
}
