<?php
namespace App;

use App\Model\AdViewer;
use App\Model\BankInterface;
use App\VO\Currency;
use App\Model\CurrencyValidatorInterface;
use App\Service\AdService;
use App\VO\Money;

class App
{
    /** @var array */
    private $config;

    /** @var AdService */
    private $service;

    /** @var AdViewer */
    private $view;

    public function __construct(
        AdService $service,
        AdViewer $view,
        BankInterface $bank,
        CurrencyValidatorInterface $currencyValidator,
        array $config = []
    ) {
        $this->service = $service;
        $this->view = $view;
        $this->config = $config;

        Currency::registerValidator($currencyValidator);
        Money::registerBank($bank);
    }

    public function showAd(int $id, string $from): string
    {
        $ad = $this->service->get($id, $from);

        return $this->view->render($ad, $this->config);
    }
}
