<?php

require_once __DIR__ .'/vendor/autoload.php';

use App\App;
use App\Service\AdManager;
use App\Model\HtmlAdViewer;
use App\Infra\FileLoggerAdapter;
use App\Infra\NullLogger;
use App\Repository\AdRepositoryFactory;
use App\Model\Bank;

$enableLogger = true;
$logger = $enableLogger ? new FileLoggerAdapter(new FileLogger('./test.log')) : new NullLogger();

$adRepositoryFactory = new AdRepositoryFactory($logger);
$bank = new Bank([
    'usd' => ['rate' => 1, 'format' => '$%g'],
    'rub' => ['rate' => 1/60, 'format' => '%g руб'],
    'eur' => ['rate' => 1.2, 'format' => '%g EUR'],
]);

$config = [
    'view_currency' => 'RUB',
];

$app = new App(new AdManager($adRepositoryFactory), new HtmlAdViewer(), $bank, $bank, $config);
$response = $app->showAd($_GET['id'] ?? 0, $_GET['from'] ?? '');

print $response;
